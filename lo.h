#ifndef SYMBOL_TABLE
#error "Trying to include lo.h withou including st.h"
#endif

/* ***************************************
 * ** Ordered linked list ****************
 * ***************************************/

/* Key and Value types should be pointers */
template <typename Key, typename Value>
class LO: public SymbolTable<Key, Value>
{
  private:
    node<Key, Value> head;
    void sort();

  public:
    LO();
    ~LO();
    void savepairs();
    /* unreference pair for deconstruction without deleting infos */
    void insere(Key, Value);
    void insertpair(kvpair<Key, Value>*);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
    node<Key, Value> * get_first();
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
LO<Key, Value>::LO() {
  head.next = nullptr;
}

template <typename Key, typename Value>
LO<Key, Value>::~LO()
{

  if (!head.next)
    return;
  node<Key, Value> *p1, *p2 = head.next;

  while ((p1 = p2)) {
    p2 = p1->next;
    delete p1;
  }
}

template <typename Key, typename Value>
void LO<Key, Value>::savepairs()
{
  node<Key, Value> *p = head.next;
  while (p) {
    p->info = nullptr;
    p = p->next;
  }
}

template <typename Key, typename Value>
void LO<Key, Value>::insere(Key key, Value val)
{
  node<Key, Value> *p = &head, *q = head.next;

  while (p->next && *(p->next->info->key) < *key) {
    p = p->next;
    q = p->next;
    if (*(p->info->key) == *key) {
      delete p->next->info->val;
      p->next->info->val = val;
      delete key;
      return;
    }
  }

  p->next = new node<Key, Value>(key, val);
  p->next->next = q;
}

template <typename Key, typename Value>
void LO<Key, Value>::insertpair(kvpair<Key, Value>* kv)
{
  /* supposes kv->info->key is not in list yet */
  node<Key, Value> *p = &head, *q = head.next;

  while (p->next && *(p->next->info->key) < *(kv->key)) {
    p = p->next;
    q = p->next;
  }

  p->next = new node<Key, Value>;
  delete p->next->info;
  p->next->info = kv;
  p->next->next = q;

}


template <typename Key, typename Value>
Value LO<Key, Value>::devolve(Key key)
{
  node<Key, Value> *p = &head;
  while ((p = p->next)) {
    if (*(p->info->key) == *key)
      return(p->info->val);
  }
  return nullptr;
}

template <typename Key, typename Value>
void LO<Key, Value>::remove(Key key)
{

  node<Key, Value> *d, *p = &head;
  while (p->next) {
    if (*(p->next->info->key) == *key) {
      d = p->next->next;
      delete p->next;
      p->next = d;
      return;
    }
    p = p->next;
  }
}

template <typename Key, typename Value>
int LO<Key, Value>::rank(Key key)
{
  int i;
  node<Key, Value> *p = &head;
  for (i = 0; (p = p->next) && *(p->info->key) < *key; i++);
  return(i);
}

template <typename Key, typename Value>
Key LO<Key, Value>::seleciona(int k)
{
  node<Key, Value> *p = head.next;

  for (int i = 0; p && i < k; i++)
    p = p->next;

  if (p)
    return p->info->key;
  return nullptr;
}

template <typename Key, typename Value>
node<Key, Value> * LO<Key, Value>::get_first()
{
  return(head.next);
}

#if DEBUG == 1
template <typename Key, typename Value>
void LO<Key, Value>::print()
{
  if (!head.next)
    return;

  node<Key, Value> *p = head.next;
  do {
    cout << *(p->info->val) << ":\t" << *(p->info->key) << "\n";
  } while ((p = p->next));

}
#endif
