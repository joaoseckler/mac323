#EP1 - Tabela de Símbolos

## Interface

Optamos por reimplementar a interface disponibilizada pelos monitores.
No entanto *os comando aceitos e a saída são exatamente os mesmos*. O
nome do executável é `ep1`, como especificado na entrega do EP.

## Estrutura do código

Cada implementação foi colocada num arquivo `.h` separado. "VD" em
`vd.h`, "VO" em `vo.h` e assim por diante. O arquivo `st.h` contém a
declaração de uma classe abstrata `SymbolTable` de que todas as demais
herdam. Em `txt` foram alguns arquivos de texto a serem usados como
teste.

## Rodando o cógido

Há um makefile, mas ele vai rodar simplesmente `g++ -Wall -g -Wpedantic
-O0 -o ep1 ep1.cpp`. Em seguida, use

`./ep1 txt/lero.txt RN`

por exemplo.

## Testando o código

Disponibilizamos um script em Bash que testa extensivamente todas as
implementações. Para cada implementação e para cada arquivo de texto, o
script testa as cinco operações disponíveis (`insere`, `devolve`,
`seleciona`, `rank` e `remove`) para todas as palavras e em ordem
aleatória.

## Resultados dos testes

Os testes foram feitos com literatura ("Lusíadas" de Camões, que não
podia faltar, um texto de Marx e outro de Poe), geradores aleatórios de
"lero-lero" e de "lorem ipsum". Segue abaixo um quadro com os
resultados.

```
*** VD ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.065087	0.0162578	0.608955	0.611669	0.0167076
poe.txt:	0.0702243	0.0216923	0.451663	0.439646	0.0228635
manifest.txt:	0.0642738	0.0449442	1.18246	1.16538	0.0637895
lorem.txt:	0.0671404	0.00329908	0.0854142	0.0831737	0.00480187
dict.txt:	0.0662506	9.50462	212.529	205.924	9.6936
lusiadas.txt:	0.0637079	0.200299	4.57088	4.42275	0.197182



*** VO ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.0929421	0.0145006	0.0199987	0.00251593	0.011157
poe.txt:	0.0918231	0.0206255	0.0166693	0.00321338	0.0222159
manifest.txt:	0.0909366	0.0547179	0.0405655	0.00468617	0.0496896
lorem.txt:	0.0942518	0.0047705	0.00387146	0.000666831	0.0047138
dict.txt:	0.105528	10.413	8.92561	1.27482	10.0389
lusiadas.txt:	0.0934979	0.217388	0.170836	0.0306847	0.199337



*** LD ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.0639663	0.0137942	0.310882	0.293432	0.0149824
poe.txt:	0.0638395	0.0177114	0.22517	0.216964	0.0197247
manifest.txt:	0.0642422	0.0596245	0.599885	0.569273	0.0603678
lorem.txt:	0.0644176	0.00343481	0.0437784	0.0417699	0.00528482
dict.txt:	0.0679332	9.6101	108.321	101.062	11.0399
lusiadas.txt:	0.0640815	0.219174	2.31268	2.17507	0.237197



*** LO ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.10635	0.0167086	0.0218555	0.0106819	0.00922623
poe.txt:	0.0993279	0.0265576	0.0169454	0.00638577	0.0241433
manifest.txt:	0.101818	0.0579894	0.0481544	0.0285892	0.0603081
lorem.txt:	0.10162	0.00452266	0.00356613	0.000756089	0.0047329
dict.txt:	0.102206	11.4855	9.7641	6.65047	10.9537
lusiadas.txt:	0.102705	0.245962	0.172495	0.138411	0.216117



*** AB ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.00684712	0.00293197	0.0162403	0.0238755	0.0074064
poe.txt:	0.00688487	0.00207703	0.00852903	0.0112685	0.00219827
manifest.txt:	0.00680756	0.0100349	0.0271279	0.0376191	0.00588776
lorem.txt:	0.00711104	0.000696667	0.00240436	0.0018724	0.000735714
dict.txt:	0.0135572	1.18835	5.27506	8.20787	1.1725
lusiadas.txt:	0.00709151	0.0380648	0.114113	0.194515	0.042711



*** TR ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.00835675	0.00295139	0.0112962	0.0177837	0.002871
poe.txt:	0.00828119	0.00204211	0.0084219	0.01384	0.00216785
manifest.txt:	0.00856709	0.0101609	0.0260255	0.0434097	0.00929221
lorem.txt:	0.00913266	0.000410233	0.00175948	0.00406613	0.00071948
dict.txt:	0.0118945	1.24417	4.96377	8.001	1.67884
lusiadas.txt:	0.00872309	0.0374923	0.0957201	0.178125	0.0291449



*** A23 ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.00702501	0.00294588	0.0104185	0.0151687	0.00304874
poe.txt:	0.00706102	0.00206888	0.00736327	0.0123714	0.00398089
manifest.txt:	0.00728958	0.0100939	0.0226993	0.0383083	0.0108166
lorem.txt:	0.00711273	0.000418042	0.00157953	0.00191304	0.000434605
dict.txt:	0.00952947	1.25278	4.41448	6.72398	1.16815
lusiadas.txt:	0.00717746	0.0316969	0.0831669	0.137746	0.0354761



*** RN ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.00661963	0.00288292	0.0111153	0.0223236	0.00555982
poe.txt:	0.0066747	0.00203185	0.00808624	0.0120907	0.00229903
manifest.txt:	0.00718219	0.00909488	0.030469	0.0461796	0.0104088
lorem.txt:	0.00669044	0.000407675	0.00172164	0.00219782	0.000416002
dict.txt:	0.00707909	1.21305	5.05167	7.74469	1.29138
lusiadas.txt:	0.00669826	0.0370713	0.0924206	0.160079	0.0217212



*** HS ***
		insere    	devolve  	rank     	seleciona	remove
lero.txt:	0.00608322	0.00307287	7.90299	20.2829	0.00524285
poe.txt:	0.00578865	0.00199318	5.78105	14.905	0.00362804
manifest.txt:	0.00577425	0.00970751	14.9452	39.1656	0.00655533
lorem.txt:	0.00545325	0.000406084	1.06652	2.96157	0.000438523
dict.txt:	0.00611332	1.38849	2701.79	7017.73	1.34539
lusiadas.txt:	0.00548442	0.0195182	55.4262	152.179	0.0262038




```


Podemos observar alguns fatos notáveis. Tabela de hash tem a inserção e
devolução mais rápidas de todas (O(1)), mas ranquear e selecionar por
índice seus elementos é muito demorado (O(n)). No teste que fizemos,
cada operação é testada em todas as entradas da tabela. Se um arquivo de
texto em cerca de 10^3 palavras; para o `rank` de cada palavra dessa, a
tabela realiza algo proporcional a 10^5 operações, que é o tamanho fixo
que escolhemos. Daí temos o que nos dá algo como 10^8 operações.

As implementações em árvore são mais rápidas para inserção e remoção que
as em lista ou vetor, mas os vetores ou listas ordenados ganham para
`rank` e `seleciona`. As implementações desordenadas sofrem para isso
pois têm que reordenar o vetor toda vez que respondem a essas chamadas.



## Dependências

*do código*
`g`++

`make` (opcionalmente)


*do teste*
`bash`

`grep`

`sed`

`sort`

`tr`

`diff`

`cut`

`valgrind` (opcionalmente)
