#include "ep1.h"
using namespace std;


int main(int argc, char ** argv)
{
  if (argc != 3) {
    print_help();
    exit (EXIT_FAILURE);
  }

  ifstream file;
  file.open(argv[1]);

  if (!file.is_open()) {
    cout << "Nome de arquivo inválido\n\n";
    print_help();
    file.close();
    exit (EXIT_FAILURE);
  }

  string impl = argv[2];
  SymbolTable<string*, int*> * st;


  if (impl == "VD") st = new VD<string*, int*>;
  else if (impl == "VO") st = new VO<string*, int*>;
  else if (impl == "LD") st = new LD<string*, int*>;
  else if (impl == "LO") st = new LO<string*, int*>;
  else if (impl == "AB") st = new AB<string*, int*>;
  else if (impl == "TR") st = new TR<string*, int*>;
  else if (impl == "A23") st = new A23<string*, int*>;
  else if (impl == "RN") st = new RN<string*, int*>;
  else if (impl == "HS") st = new HS<string*, int*>;
  else {
    cout << "Implementação inválida\n\n";
    print_help();
    exit(EXIT_FAILURE);
  }

  string *s = new string;
  wstring ws;
  int *i;
  char c;
  bool hold = false;

  locale loc("en_US.utf8");
  wstring_convert<codecvt_utf8_utf16<wchar_t>> converter;

#if MEASURE_TIME == 1
  chrono::high_resolution_clock::time_point tp = chrono::high_resolution_clock::now();
  chrono::duration<double> span;
  cout << setw(7);
#endif

  /* Find first alpha char */
  while (file.get(c) && is_not_word(c));
  file.putback(c);

  while(file.get(c)) {
    if (is_not_word(c)) {
      if (!hold) {
        if (!isalpha((*s)[0])) {
          /* Only manipulate as multybyte if first letter of the string
           * is multybyte (this supposes there are no uppercase utf
           * letter in the middle of words) */
          wstring ws = converter.from_bytes(*s);
          ws[0] = tolower(ws[0], loc);
          *s = converter.to_bytes(ws);
        }
        if ((i = st->devolve(s))) {
          (*i)++;
          delete s;
        }
        else {
          i = new int(1);
          st->insere(s, i);
        }
        s = new string();
        hold = true;
      }
    }
    else {
      s->push_back(tolower(c));
      hold = false;
    }
  }

#if MEASURE_TIME == 1
  span = chrono::duration_cast<chrono::duration<double>>(
      chrono::high_resolution_clock::now() -
      tp
      );
  cout << "Tempo de inserção: " << span.count() << "\n";
#endif

#if DEBUG == 1
  st->print();
#endif

  operation_test(st);

  file.close();
  delete s;
  delete st;
}


void print_help()
{
  cout << "Uso:\n\t./ep1 texto.txt implementação\n\n";
  cout << "Onde implementação pode ser uma das seguintes:\n";
  cout << "\tVD\n\tVO\n\tLD\n\tLO\n\tAB\n\tTR\n\tA23\n\tRN\n\tHS\n\n";
}

bool is_not_word(char c)
{
  /* This supposes there are no utf-8 punctuation marks int the text */
  return(isspace(c)
         || ispunct(c)
         || isblank(c)
         || iscntrl(c));
}


void operation_test(SymbolTable<string*, int*> * st)
{


  string line;
  string command, arg, rest;
  string *key = nullptr;
  int *val = nullptr;
  int i;
#if MEASURE_TIME == 1
  chrono::high_resolution_clock::time_point tp = chrono::high_resolution_clock::now();
  chrono::duration<double> span;
  cout << setw(6);
#endif

  string help = "Possíveis operações do teste interativo:\n"
                "\tminST\n\tdelminST\n\tgetST <chave>\n\trankST <chave>\n"
                "\tdeleteST <chave>\n\tselectST <int>\n\thelp\n"
#if MEASURE_TIME == 1
                "\tstartcount\n\tendcount\n"
#endif
#if DEBUG == 1
                "\tprint\n"
#endif
                "CRTL-D para encerrar.\n";
  cout << help << ">>> ";

  while (getline(cin, line)) {
    istringstream l(line);
    getline(l, command, ' ');
    getline(l, arg, ' ');

    if (getline(l, rest))
      cout << "WARNING: Ignorando \"" << rest << "\"\n";

    if (command.empty())
      cout << "ERROR: operação esperada\n";

    else if (command == "help" || command == "h")
      cout << help;

#if MEASURE_TIME == 1
    else if (command == "startcount") {
      tp = chrono::high_resolution_clock::now();
      cout << "Começando a contar...\n";
    }

    else if (command == "endcount") {
      span = chrono::duration_cast<chrono::duration<double>>(
          chrono::high_resolution_clock::now() -
          tp
          );
      cout << "Tempo: " << span.count() << "\n";
    }
#endif

#if DEBUG == 1
    else if (command == "print")
      st->print();
#endif

    else if (command == "minST") {
      key = st->seleciona(0);
      if (key)
        cout << *key << "\n";
      else
        cout << "ST vazia\n";
    }

    else if (command == "delminST") {
      key = st->seleciona(0);
      if (key) {
        cout << "\"" << *key;
        st->remove(key);
        cout << "\" foi removida\n";
      }
      else
        cout << "ST já está vazia\n";
    }

    else if (command == "getST") {
      if (arg.empty()) goto argempty;
      val = st->devolve(&arg);
      if (val)
        cout << arg << ": " << *val << "\n";
      else
        cout << arg << ": 0\n";
    }

    else if (command == "rankST") {
      if (arg.empty()) goto argempty;
      i = st->rank(&arg);
      if (i == -1)
        cout << "A chave " << arg << " não está na tabela\n";
      else
        cout << i << "\n";
    }

    else if (command == "deleteST") {
      if (arg.empty()) goto argempty;
      st->remove(&arg);
      cout << "\"" << arg << "\" foi removida\n";
    }

    else if (command == "selectST") {
      try {
        i = stoi(arg);
      }
      catch (invalid_argument& ia) {
        cerr << "ERROR: operação necessita um número.\n";
        goto prompt;
      }

      key = st->seleciona(i);
      if (key)
        cout << "Posição " << arg << " = " << *key << "\n";
      else
        cout << "Argumento maior do que o número de entradas na tabela\n";
    }

    else
      cerr << "ERROR: Forneça um comando válido. Liste opções com help\n";


    prompt:
    cout << ">>> ";
    continue;

    argempty:
    cerr << "ERROR: operação necessita uma palavra\n";
    goto prompt;
  }
}

