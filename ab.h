#ifndef SYMBOL_TABLE
#error "Trying to include ab.h withou including st.h"
#endif

/* ***************************************
 * ** Binary Tree ************************
 * ***************************************/

template <typename Key, typename Value>
struct abnode_s {
  abnode_s(Key key = nullptr, Value val = nullptr) {
    info.key = key;
    info.val = val;
    left = nullptr;
    right = nullptr;
  }

  kvpair<Key, Value> info;
  abnode_s * left;
  abnode_s * right;
};

template <typename Key, typename Value>
using abnode = struct abnode_s<Key, Value>;


/* Key and Value types should be pointers */
template <typename Key, typename Value>
class AB: public SymbolTable<Key, Value>
{
  private:
    abnode<Key, Value> *root;
    void delnodes(abnode<Key, Value> *p);
    abnode<Key, Value> *merge(abnode<Key, Value>*, abnode<Key, Value>*);
    abnode<Key, Value> *removenode(abnode<Key, Value>*, Key);
    int countnodes(abnode<Key, Value>*);
    void rprint(abnode<Key, Value>*, int);
  public:
    AB();
    ~AB();
    void insere(Key, Value);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
AB<Key, Value>::AB(): root(nullptr) {}

template <typename Key, typename Value>
AB<Key, Value>::~AB()
{
  delnodes(root);
}

template <typename Key, typename Value>
void AB<Key, Value>::delnodes(abnode<Key, Value> *p)
{
  if (p) {
    delnodes(p->left);
    delnodes(p->right);
    delete p;
  }
}

template <typename Key, typename Value>
void AB<Key, Value>::insere(Key key, Value val)
{
  abnode<Key, Value> *p = root, **q = &root;

  while (p) {
    if (*key < *(p->info.key)) {
      q = &(p->left);
      p = p->left;
    }

    else if (*key > *(p->info.key)) {
      q = &(p->right);
      p = p->right;
    }

    else {
      delete p->info.val;
      p->info.val = val;
      delete key;
      return;
    }
  }
  *q = new abnode<Key, Value>(key, val);
}

template <typename Key, typename Value>
Value AB<Key, Value>::devolve(Key key)
{

  abnode<Key, Value> *p = root;
  while (p) {
    if (*key < *(p->info.key))
      p = p->left;
    else if (*key > *(p->info.key))
      p = p->right;
    else
      return (p->info.val);

  }
  return nullptr;
}

template <typename Key, typename Value>
void AB<Key, Value>::remove(Key key)
{

  abnode<Key, Value> *p = root, **q = &root;

  while (p) {
    if (*key < *(p->info.key)) {
      q = &(p->left);
      p = p->left; // = removenode(p->left, key);
    }

    else if (*key > *(p->info.key)) {
      q = &(p->right);
      p = p->right; // = removenode(p->right, key);
    }

    else { /* Delete p */
      *q = merge(p->left, p->right);
      delete p;
      return;
    }
  }
}

template <typename Key, typename Value>
abnode<Key, Value> * AB<Key, Value>::merge(abnode<Key, Value> *left,
                                           abnode<Key, Value> *right)
{
  if (!left)
    return(right);
  if (!right)
    return(left);

  left->right = merge(left->right, right);
  return left;
}


template <typename Key, typename Value>
abnode<Key, Value> * AB<Key, Value>::removenode(abnode<Key, Value> *p, Key key)
{
}

template <typename Key, typename Value>
int AB<Key, Value>::rank(Key key)
{
  abnode<Key, Value> *p = root;
  int r = 0;

  while (p) {
    if (*key < *(p->info.key)) {
      p = p->left;
    }
    else if (*key > *(p->info.key)) {
      r = r + 1 + countnodes(p->left);
      p = p->right;
    }
    else
      return r + countnodes(p->left);
  }
  return r;
}

template <typename Key, typename Value>
Key AB<Key, Value>::seleciona(int k)
{
  abnode<Key, Value> *p = root;
  int r = countnodes(p->left);

  while (p) {
    if (r > k) {
      p = p->left;
      if (p)
        r = r - 1 - countnodes(p->right);
    }
    else if (r < k) {
      p = p->right;
      if (p)
        r = r + 1 + countnodes(p->left);
    }
    else
      return p->info.key;
  }
  return nullptr;
}

template <typename Key, typename Value>
int AB<Key, Value>::countnodes(abnode<Key, Value> *p)
{
  if (p)
    return 1 + countnodes(p->left) + countnodes(p->right);
  else
    return 0;
}


#if DEBUG == 1
template <typename Key, typename Value>
void AB<Key, Value>::print()
{
  rprint(root, 0);
}

template <typename Key, typename Value>
void AB<Key, Value>::rprint(abnode<Key, Value> *p, int space)
{
  if (!p)
    return;
  rprint(p->right, space + 1);

  cout << "\n";
  for (int i = 0; i < space; i++)
    cout << "\t";
  cout << *(p->info.val) << ": " << *(p->info.key) << "\n";

  rprint(p->left, space + 1);
}
#endif
