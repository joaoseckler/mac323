# EP1 - Symbol Table

This program builds a symbol table which counts word occurences in text
files. Besides choosing which text file as input, the user can choose
among several symbol table implementations:

VD: unordered array

VO: ordered array

LD: unordered linked list

LO: ordered linked list

AB: binary search tree

TR: treap

A23: 2-3 tree

RN: red-black tree

HS: hash table

Usage: `$ make && ./ep1 textfile.txt [impl]` where `impl` can be one of
the above.

There are several sample txt files in the `txt` folder.

After building the st, the program offers a command line interface for
tweaking with it. The accepted commands are:

`minST`

`delminST`

`getST` <chave>

`rankST` <chave>

`deleteST` <chave>

`selectST` <int>

`help`

`print`

`startcount`

`endcount`

Most of them are intuitive. Rank returns how many elements in the table
are smaller than it's argument (i.e. in which position the key would be
inserted). The last two are used for measuring performance: they count
time. These are only availale if `MEASURE_TIME` is enabled in `ep1.h`
befor compilation. Similarly, `print` is only available when `DEBUG` is
enabled in `st.h`.


## Dependencies

*Running*

`g`++

`make` (optionally)


*testing*

`bash`

`grep`

`sed`

`sort`

`tr`

`diff`

`cut`

`valgrind` (opcionalmente)


## TODO

-Remake "seleciona" and "rank" for tree implementations: track height to
avoid recursion


