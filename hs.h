#ifndef SYMBOL_TABLE
#error "Trying to include hs.h withou including st.h"
#endif

#include <functional>

/* ***************************************
 * ** Hash Table *************************
 * ***************************************/

#define V_SIZE 1000000

/* Key and Value types should be pointers */
template <typename Key, typename Value>
class HS: public SymbolTable<Key, Value>
{
  private:
    LO<Key, Value> ** v;
    int h(Key k);
  public:
    HS();
    ~HS();
    void insere(Key, Value);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
HS<Key, Value>::HS(): v(new LO<Key, Value>*[V_SIZE])
{
  for (int i = 0; i < V_SIZE; i++)
    v[i] = new LO<Key, Value>;
}

template <typename Key, typename Value>
HS<Key, Value>::~HS()
{
  for (int i = 0; i < V_SIZE; i++) {
    delete v[i];
  }
  delete [] v;
}

template <typename Key, typename Value>
void HS<Key, Value>::insere(Key key, Value val)
{
  v[h(key)]->insere(key, val);
}

template <typename Key, typename Value>
Value HS<Key, Value>::devolve(Key key)
{
  return v[h(key)]->devolve(key);
}

template <typename Key, typename Value>
void HS<Key, Value>::remove(Key key)
{
  v[h(key)]->remove(key);
}

template <typename Key, typename Value>
int HS<Key, Value>::rank(Key key)
{

  node<Key, Value> *p;
  int r = 0;
  for (int i = 0; i < V_SIZE; i++) {
    p = v[i]->get_first();

    while (p) {
      if (*(p->info->key) < *key)
        r++;
      p = p->next;
    }
  }
  return (r);
}

template <typename Key, typename Value>
Key HS<Key, Value>::seleciona(int k)
{
  LO<Key, Value> *tmplist = new LO<Key, Value>;
  node<Key, Value> *p;

  for (int i = 0; i < V_SIZE; i++) {
    p = v[i]->get_first();
    while (p) {
      tmplist->insertpair(p->info);
      p = p->next;
    }
  }

  Key key = tmplist->seleciona(k);
  tmplist->savepairs();
  delete tmplist;
  return(key);
}

template <typename Key, typename Value>
int HS<Key, Value>::h(Key k)
{
  int h = 5831;
  for (char const &c: *k)
    h = ((h * 33) ^ c) % V_SIZE;
  if (h < 0)
    return -h;
  return h;
}

#if DEBUG == 1
template <typename Key, typename Value>
void HS<Key, Value>::print()
{

  for (int i = 0; i < V_SIZE; i++)
    v[i]->print();
}
#endif
