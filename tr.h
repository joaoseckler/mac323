#ifndef SYMBOL_TABLE
#error "Trying to include tr.h withou including st.h"
#endif

#include <random>

/* ***************************************
 * ** Treap ******************************
 * ***************************************/

template <typename Key, typename Value>
struct trnode_s {
  trnode_s(Key key = nullptr, Value val = nullptr, int pri = 0) {
    info.key = key;
    info.val = val;
    priority = pri;
    left = nullptr;
    right = nullptr;
  }

  kvpair<Key, Value> info;
  trnode_s * left;
  trnode_s * right;
  int priority;
};

template <typename Key, typename Value>
using trnode = struct trnode_s<Key, Value>;


/* Key and Value types should be pointers */
template <typename Key, typename Value>
class TR: public SymbolTable<Key, Value>
{
  private:
    trnode<Key, Value> *root;

    /* Helper (generally recursive) functions */
    void delnodes(trnode<Key, Value> *p);
    int countnodes(trnode<Key, Value>*);
    void split(trnode<Key, Value> *p,
               Key,
               trnode<Key, Value> **,
               trnode<Key, Value> **);
    trnode<Key, Value> * merge(trnode<Key, Value> *,
                               trnode<Key, Value> *);
    void rprint(trnode<Key, Value>*, int);

    /* Random generation */
    mt19937 gen;
    uniform_int_distribution<int> uid;

  public:
    TR();
    ~TR();
    void insere(Key, Value);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
TR<Key, Value>::TR(): root(nullptr) {
  random_device dev;
  gen = mt19937(dev());
}

template <typename Key, typename Value>
TR<Key, Value>::~TR()
{
  delnodes(root);
}

template <typename Key, typename Value>
void TR<Key, Value>::delnodes(trnode<Key, Value> *p)
{
  if (p) {
    delnodes(p->left);
    delnodes(p->right);
    delete p;
  }
}

template <typename Key, typename Value>
void TR<Key, Value>::insere(Key key, Value val)
{

  int pri = uid(gen), ppriority;
  trnode<Key, Value> *p = root, **q = &root, **t = &root;

  while (p) {
    ppriority = p->priority;
    if (*key < *(p->info.key)) {
      q = &(p->left);
      p = p->left;
    }

    else if (*key > *(p->info.key)) {
      q = &(p->right);
      p = p->right;
    }
    else {
      delete p->info.val;
      p->info.val = val;
      delete key;
      return;
    }

    if (pri >= ppriority && *t == root) {
      /* t is the address of the pointer to the node where a new node
       * must be entered. *t == root guarantees this is only executed
       * the first time pri >= ppriority */
      t = q;
    }
  }

  if (*t) {
    trnode<Key, Value> *n = new trnode<Key, Value>(key, val, pri);
    split(*t, key, &(n->left), &(n->right));
    *t = n;
  }

  else
    *t = new trnode<Key, Value>(key, val, pri);
}

template <typename Key, typename Value>
void TR<Key, Value>::split(trnode<Key, Value> *p,
                           Key key,
                           trnode<Key, Value> **left,
                           trnode<Key, Value> **right)
{
  if (!p)
    *left = *right = nullptr;
  else if (*key < *(p->info.key)) {
    split(p->left, key, left, &(p->left));
    *right = p;
  }
  else {
    split(p->right, key, &(p->right), right);
    *left = p;
  }
}

template <typename Key, typename Value>
Value TR<Key, Value>::devolve(Key key)
{

  trnode<Key, Value> *p = root;
  while (p) {
    if (*key < *(p->info.key))
      p = p->left;
    else if (*key > *(p->info.key))
      p = p->right;
    else
      return (p->info.val);

  }
  return nullptr;
}

template <typename Key, typename Value>
void TR<Key, Value>::remove(Key key)
{
  trnode<Key, Value> *p = root, **q = &root;

  while (p) {
    if (*key < *(p->info.key)) {
      q = &(p->left);
      p = p->left;
    }

    else if (*key > *(p->info.key)) {
      q = &(p->right);
      p = p->right;
    }

    else {
      *q = merge(p->left, p->right);
      delete p;
      return;
    }
  }
}

template <typename Key, typename Value>
trnode<Key, Value> * TR<Key, Value>::merge(trnode<Key, Value> *left,
                                           trnode<Key, Value> *right)
{
  if (!left)
    return(right);
  if (!right)
    return(left);

  if (left->priority > right->priority) {
    left->right = merge(left->right, right);
    return left;
  }
  else {
    right->left = merge(right->left, left);
    return right;
  }
}

template <typename Key, typename Value>
int TR<Key, Value>::rank(Key key)
{
  trnode<Key, Value> *p = root;
  int r = 0;

  while (p) {
    if (*key < *(p->info.key)) {
      p = p->left;
    }
    else if (*key > *(p->info.key)) {
      r = r + 1 + countnodes(p->left);
      p = p->right;
    }
    else
      return r + countnodes(p->left);
  }
  return r;
}

template <typename Key, typename Value>
Key TR<Key, Value>::seleciona(int k)
{
  if (!root)
    return nullptr;

  trnode<Key, Value> *p = root;
  int r = countnodes(p->left);

  while (p) {
    if (r > k) {
      p = p->left;
      if (p)
        r = r - 1 - countnodes(p->right);
    }
    else if (r < k) {
      p = p->right;
      if (p)
        r = r + 1 + countnodes(p->left);
    }
    else
      return p->info.key;
  }
  return nullptr;
}

template <typename Key, typename Value>
int TR<Key, Value>::countnodes(trnode<Key, Value> *p)
{
  if (p)
    return 1 + countnodes(p->left) + countnodes(p->right);
  else
    return 0;
}


#if DEBUG == 1
template <typename Key, typename Value>
void TR<Key, Value>::print()
{
  rprint(root, 0);
}

template <typename Key, typename Value>
void TR<Key, Value>::rprint(trnode<Key, Value> *p, int space)
{
  if (!p)
    return;
  rprint(p->right, space + 1);

  cout << "\n";
  for (int i = 0; i < space; i++)
    cout << "\t";
  cout << *(p->info.val) << ": " << *(p->info.key) << "\n";

  rprint(p->left, space + 1);
}
#endif
