CPPFLAGS=-Wall -g -Wpedantic -O0
INCLUDE=ep1.h st.h vd.h vo.h ld.h lo.h ab.h tr.h a23.h rn.h hs.h

ep1: ep1.cpp $(INCLUDE)
	g++ $(CPPFLAGS) -o $@ $<

.PHONY: clean
clean:
	rm -rf *.o ep1 tests/
