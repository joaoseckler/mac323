#ifndef SYMBOL_TABLE
#error "Trying to include ld.h withou including st.h"
#endif

/* ***************************************
 * ** Unordered linked list **************
 * ***************************************/

template <typename Key, typename Value>
struct node_s {
  node_s(Key key = nullptr, Value val = nullptr) {
    info = new kvpair<Key, Value>(key, val);
    next = nullptr;
  }
  ~node_s() {
    delete info;
  }

  kvpair<Key, Value> *info;
  node_s * next;
};

template <typename Key, typename Value>
using node = struct node_s<Key, Value>;


/* Key and Value types should be pointers */
template <typename Key, typename Value>
class LD: public SymbolTable<Key, Value>
{
  private:
    node<Key, Value> head;
    void sort();
    node<Key, Value> *mergesort(node<Key, Value>*, int);
    node<Key, Value> *merge(node<Key, Value>*, node<Key, Value>*);

    int size;
  public:
    LD();
    ~LD();
    void insere(Key, Value);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
LD<Key, Value>::LD(): size(0) {}

template <typename Key, typename Value>
LD<Key, Value>::~LD()
{

  if (!head.next)
    return;
  node<Key, Value> *p1, *p2 = head.next;

  while ((p1 = p2)) {
    p2 = p1->next;
    delete p1;
  }
}

template <typename Key, typename Value>
void LD<Key, Value>::insere(Key key, Value val)
{
  node<Key, Value> *p = &head;

  if (head.next) {
    do {
      if (*(p->next->info->key) == *key) {
        delete p->next->info->val;
        p->next->info->val = val;
        delete key;
        return;
      }
    } while ((p = p->next)->next);
  }

  p->next = new node<Key, Value>(key, val);
  size++;
}

template <typename Key, typename Value>
Value LD<Key, Value>::devolve(Key key)
{

  node<Key, Value> *p = &head;
  while ((p = p->next)) {
    if (*(p->info->key) == *key)
      return(p->info->val);
  }
  return nullptr;
}

template <typename Key, typename Value>
void LD<Key, Value>::remove(Key key)
{

  node<Key, Value> *d, *p = &head;
  while (p->next) {
    if (*(p->next->info->key) == *key) {
      d = p->next->next;
      delete p->next;
      p->next = d;
      size--;
      return;
    }
    p = p->next;
  }
}

template <typename Key, typename Value>
int LD<Key, Value>::rank(Key key)
{
  int i;
  sort();
  node<Key, Value> *p = &head;
  for (i = 0; (p = p->next) && *(p->info->key) < *key; i++);
  return(i);
}

template <typename Key, typename Value>
Key LD<Key, Value>::seleciona(int k)
{
  sort();
  node<Key, Value> *p = head.next;
  if (k >= size)
    return nullptr;

  for (int i = 0; i < k; i++)
    p = p->next;
  return p->info->key;
}

template <typename Key, typename Value>
void LD<Key, Value>::sort()
{
  head.next = mergesort(head.next, size);
}

template <typename Key, typename Value>
node<Key, Value> * LD<Key, Value>::mergesort(node<Key, Value> *h, int s)
{

  if (!h || s == 1)
    return h;
  int i;
  node<Key, Value> *p = h, *left = h, *right;

  /* Split left and right */
  for (i = 0; i < s/2 - 1; i++)
    p = p->next;
  i++;
  right = p->next;
  p->next = nullptr;
  /************************/

  left = mergesort(left, i);
  right = mergesort(right, s - i);

  /* merge */
  return merge(left, right);
}

template <typename Key, typename Value>
node<Key, Value> *LD<Key, Value>::merge(node<Key, Value> *l,
                                        node<Key, Value> *r)
{
  if (!l)
    return(r);
  if (!r)
    return(l);

  node<Key, Value> *res;
  if (*(l->info->key) <= *(r->info->key)) {
    res = l;
    res->next = merge(l->next, r);
  }
  else {
    res = r;
    res->next = merge(l, r->next);
  }
  return(res);
}

#if DEBUG == 1
template <typename Key, typename Value>
void LD<Key, Value>::print()
{
  if (!head.next)
    return;

  node<Key, Value> *p = head.next;
  do {
    cout << *(p->info->val) << ":\t" << *(p->info->key) << "\n";
  } while ((p = p->next));

}
#endif
