#ifndef SYMBOL_TABLE
#error "Trying to include vo.h withou including st.h"
#endif

/* ***************************************
 * ** Ordered array ********************
 * ***************************************/

/* Key and Value types should be pointers */
template <typename Key, typename Value>
class VO: public SymbolTable<Key, Value>
{
  private:
    int size;
    kvpair<Key, Value> ** v;
    int n;
    void resize(int);
  public:
    VO();
    ~VO();
    void insere(Key, Value);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
VO<Key, Value>::VO(): size(1), v(new kvpair<Key, Value>*[1]), n(0) {}

template <typename Key, typename Value>
VO<Key, Value>::~VO()
{
  for (int i = 0; i < n; i++) {
    delete v[i];
  }
  delete [] v;
}

template <typename Key, typename Value>
void VO<Key, Value>::insere(Key key, Value val) {

  int r = rank(key);

  if (r < n && *(v[r]->key) == *(key)) {
    delete v[r]->val;
    v[r]->val = val;
    delete key;
    return;
  }
  else {
    if (n == size)
      resize(size*2);
    for (int j = n - 1; j >= r; j--) {
      v[j + 1] = v[j];
      v[j + 1] = v[j];
    }
    v[r] = new kvpair<Key, Value>(key, val);
    n++;
  }
}

template <typename Key, typename Value>
Value VO<Key, Value>::devolve(Key key) {
  for (int i = 0; i < n; i++) {
    if (*(v[i]->key) == *(key))
      return v[i]->val;
  }
  return nullptr;
}

template <typename Key, typename Value>
void VO<Key, Value>::remove(Key key) {

  for (int i = 0; i < n; i++)
    if (*(v[i]->key) == *(key)) {
      delete v[i];

      for (; i < n - 1; i++) {
        v[i] = v[i + 1];
        v[i] = v[i + 1];
      }
      n--;
      return;
    }

}

template <typename Key, typename Value>
int VO<Key, Value>::rank(Key key) {
  int i;
  for (i = 0; i < n && *(v[i]->key) < *key; i++);
  return(i);
}

template <typename Key, typename Value>
Key VO<Key, Value>::seleciona(int k) {
  if (k < n)
    return v[k]->key;
  return nullptr;

}

template <typename Key, typename Value>
void VO<Key, Value>::resize(int t)
{
  kvpair<Key, Value> ** newv = new kvpair<Key, Value>*[t];
  for (int i = 0; i < n; i++)
    newv[i] = v[i];
  delete [] v;
  v = newv;
  size = t;
}

#if DEBUG == 1
template <typename Key, typename Value>
void VO<Key, Value>::print()
{
  for (int i = 0; i < n; i++)
    if (v[i]->val && v[i]->key)
      cout << *(v[i]->val) << ":\t" << *(v[i]->key) << "\n";
}
#endif
