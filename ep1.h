#include <cstdlib>
#include <fstream>
#include <string>
#include <locale>
#include <codecvt>
#include <sstream>

#include "st.h" // iostream, algorithm
#include "vd.h"
#include "vo.h"
#include "ld.h"
#include "lo.h"
#include "ab.h"
#include "tr.h"
#include "a23.h"
#include "rn.h"
#include "hs.h"

#define MEASURE_TIME 1

#if MEASURE_TIME == 1
#include <chrono>
#include <iomanip>
#endif

void print_help();
bool is_not_word(char);
void operation_test(SymbolTable<string*, int*> *);
