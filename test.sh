#!/bin/bash
# set -x
# trap read debug

if [ "$#" -gt 1 -o "$1" = "--help" -o \( "$1" -a "$1" != "--valgrind" \) ]; then
  echo "Usage: test.sh [--valgrind | --help]"
  echo -e "\t--valgrind will run valgrind with --leak-check=full and check for memory errors"
  echo -e "\t--help will display this message"
  exit 1
fi

valgrind=""
if [ "$1" = "--valgrind" ]; then
  valgrind="valgrind --leak-check=full"
fi

mkdir -p tests

impl=( VD VO LD LO AB TR A23 RN )
read_test_files=( "lero.txt" )
# read_test_files=( "lero.txt" )

# Delete must be the last one
test_types=( "getST" "rankST" "selectST" "deleteST" )
# operations should be the portuguese names of the above commands plus
# "insere" at the beggining
operations=( "insere    " "devolve  " "rank     " "seleciona" "remove  " )

echo
echo "Making..."
make &> tests/make-out.txt
if cat tests/make-out.txt 2>&1 | grep -q "error" -i; then
  echo "Found error while running make. Exiting..."
  exit 1

elif cat tests/make-out.txt | grep -q "warning" -i; then
  echo "Found make warnings. Continuing..."
fi
echo

for read_file in "${read_test_files[@]}"; do

  # List of words, delete all but alnum chars, all to lowercase (utf-8
  # aware) and finally sort randomly deleting duplicates
  cat txt/${read_file} | tr ' ' '\n' | sed 's/[^[:alnum:]]//g' |
  sed 's/.*/\L&/' | sort -bduR > tests/words.txt

  echo > tests/${read_file}-commands.txt

  # Create one file of commands for each command type
  for tt in ${test_types[@]}; do
    echo "startcount" >> tests/${read_file}-commands.txt

    if [ "selectST" = "$tt" ]; then # Select uses an int as argument
      i=0
      while read w; do
        echo "$tt $i" >> tests/${read_file}-commands.txt
        ((++i))
      done < tests/words.txt
    else
      while read w; do
        echo "$tt $w" >> tests/${read_file}-commands.txt
      done < tests/words.txt
    fi

    echo "endcount" >> tests/${read_file}-commands.txt
  done

  for st in "${impl[@]}"; do
    echo Testing $st with $read_file...
    $valgrind ./ep1 txt/$read_test_files $st \
      < tests/${read_file}-commands.txt \
      2> tests/stderr-${st}-${read_file}.diff |
      tee tests/${st}-${read_file}.diff |
      egrep "^(>>> )?Tempo" > tests/time-${st}-${read_file}.diff

    grep -Fvx -f tests/time-${st}-${read_file}.diff\
      tests/${st}-${read_file}.diff > tests/tmp.txt &&
      mv tests/tmp.txt tests/${st}-${read_file}.diff
  done
done

echo
echo "Diffing every combination of two outputs..."
echo

for read_file in "${read_test_files[@]}"; do
  for ((i = 0; i < ${#impl[@]}; i++ )); do
    for ((j = $i + 1; j < ${#impl[@]}; j++ )); do
      diff -q tests/${impl[$i]}-$read_file.diff tests/${impl[$j]}-$read_file.diff
    done
  done
done

if [ "$valgrind" ]; then

  echo
  echo "Verifying memory errors..."
  echo


  for read_file in "${read_test_files[@]}"; do
    for st in "${impl[@]}"; do

      if ! cat tests/stderr-${st}-${read_file}.diff \
        | grep -q "0 errors from"; then

        echo "Memory errors for ${st}-${read_file}." \
             "Errors output in file \"stderr-${st}-${read_file}.diff\""

      elif ! cat tests/stderr-${st}-${read_file}.diff \
        | grep -q "All heap blocks were freed"; then

        echo "Memory leak for ${st}-${read_file}." \
             "Error output in file \"stderr-${st}-${read_file}.diff\""
      fi
    done
  done

fi

for st in "${impl[@]}"; do
  echo "*** $st ***"

  echo -en "\t\t"
  for op in "${operations[@]}"; do
    echo -ne "${op}\t"
  done
  echo

  for read_file in "${read_test_files[@]}"; do
    echo -ne "$read_file:\t"
    for i in "${!operations[@]}"; do
      cat tests/time-${st}-${read_file}.diff |
        cut -d':' -f 2 | cut -d' ' -f 2 |
        head -$(($i + 1)) | tail -1 | tr -d '\n'
      echo -ne "\t"
    done
    echo -e "\n"
  done
  echo -e "\n\n\n"
done
