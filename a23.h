#ifndef SYMBOL_TABLE
#error "Trying to include a23.h without including st.h"
#endif

/* ***************************************
 * ** 2-3 Tree ***************************
 * ***************************************/

template <typename Key, typename Value>
struct a23node_s {
  a23node_s(kvpair<Key, Value> * kv = nullptr) {
    linfo = kv;
    rinfo = nullptr;
    left = nullptr;
    middle = nullptr;
    right = nullptr;
    father = nullptr;
  }
  ~a23node_s() {
    delete linfo;
    delete rinfo;
  }

  kvpair<Key, Value> *linfo;
  kvpair<Key, Value> *rinfo;
  a23node_s * left;
  a23node_s * middle;
  a23node_s * right;
  a23node_s * father;
};

template <typename Key, typename Value>
using a23node = struct a23node_s<Key, Value>;


/* Key and Value types should be pointers */
template <typename Key, typename Value>
class A23: public SymbolTable<Key, Value>
{
  private:
    a23node<Key, Value> *root;
    void delnodes(a23node<Key, Value> *p);
    int countnodes(a23node<Key, Value>*);
    void rprint(a23node<Key, Value>*, int);
  public:
    A23();
    ~A23();
    void insere(Key, Value);
    Value devolve(Key);
    void remove(Key);
    int rank(Key);
    Key seleciona(int);
#if DEBUG == 1
    void print();
#endif
};

template <typename Key, typename Value>
A23<Key, Value>::A23(): root(nullptr) {}

template <typename Key, typename Value>
A23<Key, Value>::~A23()
{
  delnodes(root);
}

template <typename Key, typename Value>
void A23<Key, Value>::delnodes(a23node<Key, Value> *p)
{
  if (p) {
    delnodes(p->left);
    delnodes(p->middle);
    delnodes(p->right);
    delete p;
  }
}

template <typename Key, typename Value>
void A23<Key, Value>::insere(Key key, Value val)
{
  a23node<Key, Value> *p = root, *q = root;

  /* Look the tree down until we find the desired key already inserted
   * or until we find the leaf where it should be inserted */
  while (p) {
    q = p;
    if (*key < *(p->linfo->key))
      p = p->left;
    else if (*key > *(p->linfo->key)) {
      if (!(p->rinfo) || *key < *(p->rinfo->key))
        p = p->middle;

      else if (*key > *(p->rinfo->key))
        p = p->right;

      else {
        delete p->rinfo->val;
        p->rinfo->val = val;
        delete key;
        return;
      }
    }

    else {
      delete p->linfo->val;
      p->linfo->val = val;
      delete key;
      return;
    }
  }

  /* q points to the node where insertion must be made. We look up the
   * tree until we find the root (q == nullptr) or we find a 2-node.
   * Each 3-node found must be split into two (the already existing q
   * and a new one, p), and those are passed to the next iteration as l
   * and r. */

  kvpair<Key, Value> *kv = new kvpair<Key, Value>(key, val);
  a23node<Key, Value> *l = nullptr, *r = nullptr;

  while (q && q->rinfo) {

    p = new a23node<Key, Value>;

    if (*(kv->key) > *(q->rinfo->key)) {
      p->linfo = kv;
      kv = q->rinfo;
      p->left = l;
      p->middle = r;
      /* q->left = q->left */
      /* q->middle = q->middle */
      if (l) {
        l->father = p;
        r->father = p;
      }
    }

    else if (*(kv->key) < *(q->linfo->key)) {
      p->linfo = kv; /* using p->linfo as a temporary value */
      kv = q->linfo;
      q->linfo = p->linfo;
      p->linfo = q->rinfo;

      p->left = q->middle;
      p->middle = q->right;
      if (p->left) {
        p->left->father = p;
        p->middle->father = p;
      }
      q->left = l;
      q->middle = r;
      if (l) {
        l->father = q;
        r->father = q;
      }
    }

    else {
      /* kv remains the same */
      p->linfo = q->rinfo;

      p->left = r;
      p->middle = q->right;
      if (p->middle)
        p->middle->father = p;
      /* q->left = q->left */
      q->middle = l;
      if (l) {
        l->father = q;
        r->father = p;
      }
    }


    l = q;
    r = p;
    q->right = nullptr;
    q->rinfo = nullptr;
    q = q->father;
  }

  if (q) { /* q is a 2-node */
    if (*(kv->key) > *(q->linfo->key)) { /* Insert to the right */
      q->rinfo = kv;
      q->middle = l;
      q->right = r;
    }

    else { /* Insert to the left */
      q->rinfo = q->linfo;
      q->linfo = kv;
      q->right = q->middle;
      q->left = l;
      q->middle = r;
    }
    if (r) r->father = q;
  }

  else { /* q is null => reached root */
    root = new a23node<Key, Value>;
    root->linfo = kv;
    root->left = l;
    root->middle = r;
    if (root->left) {
      root->left->father = root;
      root->middle->father = root;
      if (r->left) {
         r->left->father = r;
         r->middle->father = r;
      }
    }
  }
}

template <typename Key, typename Value>
Value A23<Key, Value>::devolve(Key key)
{
  a23node<Key, Value> *p = root;

  while (p) {
    if (*key < *(p->linfo->key))
      p = p->left;
    else if (*key > *(p->linfo->key)) {
      if (!(p->rinfo) || *key < *(p->rinfo->key))
        p = p->middle;

      else if (*key > *(p->rinfo->key))
        p = p->right;

      else {
        return(p->rinfo->val);
      }
    }

    else {
      return(p->linfo->val);
    }
  }
  return nullptr;
}

template <typename Key, typename Value>
void A23<Key, Value>::remove(Key key)
{

  a23node<Key, Value> *p = root;
  kvpair<Key, Value> *tmp;

  while (p) {
    if (*key < *(p->linfo->key))
      p = p->left;

    else if (*key > *(p->linfo->key)) {
      if (!(p->rinfo) || *key < *(p->rinfo->key))
        p = p->middle;

      else if (*key > *(p->rinfo->key))
        p = p->right;

      else { /* found key in right info */
        if (p->left) { /* is not leaf */
          tmp = p->rinfo;
          p->rinfo = p->right->linfo;
          p->right->linfo = tmp;
          p = p->right;
        }
        else { /* is leaf */
          delete p->rinfo;
          p->rinfo = nullptr;
          return;
        }
      }
    }

    else { /* found key in left info */
      if (p->left) { /* is not leaf */
        tmp = p->linfo;
        p->linfo = p->middle->linfo;
        p->middle->linfo = tmp;
        p = p->middle;
      }
      else { /* is leaf */
        if (p->rinfo) { /* if p is 3-node, simple */
          delete p->linfo;
          p->linfo = p->rinfo;
          p->rinfo = nullptr;
          return;
        }

        /* At this point p is a 2-node */

        /* Check if sibling is 3-node. If yes, swap infos around until
         * the 2-3 properties are preserved. If not, check if father is
         * 3-node. If yes, delete the correct son and pass one of the
         * values of the father to the children. If not, merge the
         * father with the remaining children and recurse upwards to
         * bring one node down. */

        /* Convention: any sons p has are in p->left */

        /* In the cases below there are expressions common to more than
         * one block, and to avoid code duplication they are put in an
         * area where they can be executed only once. The code is way
         * more readable, however, if each case contains all the
         * instructions required by it, so we mantained those lines as
         * comments inside those cases blocks. That is why the following
         * code has so many comments. */

        a23node<Key, Value> *f = p->father;
        delete p->linfo;

        while (f) {

          /* Check if p has 3-node siblings */
          if (f->middle->rinfo) {
            if (p == f->right) {
              p->linfo = f->rinfo;
              p->middle = p->left;
              f->rinfo = f->middle->rinfo;
              p->left = f->middle->right;
              if (p->left)
                p->left->father = p;
            }
            else { /* p = f->left */
              p->linfo = f->linfo;
              f->linfo = f->middle->linfo;
              f->middle->linfo = f->middle->rinfo;
              p->middle = f->middle->left;
              if (p->middle)
                p->middle->father = p;
              f->middle->left = f->middle->middle;
              f->middle->middle = f->middle->right;
            }
            f->middle->rinfo = nullptr;
            f->middle->right = nullptr;
            return;
          }

          if (f->left->rinfo) {
            p->middle = p->left;
            if (p == f->middle) {
              p->linfo = f->linfo;
              /* f->linfo = f->left->rinfo; */
              /* f->left->rinfo = nullptr; */
              /* p->middle = p->left; */
              p->left = f->left->right;
              /* if (p->left) */
              /*   p->left->father = p; */
              /* f->left->right = nullptr; */
            }
            else { /* p == f->right */
              p->linfo = f->rinfo;
              /* p->middle = p->left; */
              f->rinfo = f->middle->linfo;
              p->left = f->middle->middle;
              /* if (p->left) */
              /*   p->left->father = p; */
              f->middle->linfo = f->linfo;
              f->middle->middle = f->middle->left;
              /* f->linfo = f->left->rinfo; */
              /* f->left->rinfo = nullptr; */
              f->middle->left = f->left->right;
              if (f->middle->left)
                f->middle->left->father = f->middle;
              /* f->left->right = nullptr; */
            }
            f->linfo = f->left->rinfo;
            f->left->rinfo = nullptr;
            f->left->right = nullptr;
            if (p->left)
              p->left->father = p;
            return;
          }


          if (f->right && f->right->rinfo) {
            if (p == f->left) {
              p->linfo = f->linfo;
              f->linfo = f->middle->linfo;
              p->middle = f->middle->left;
              /* if (p->middle) */
              /*   p->middle->father = p; */
              f->middle->left = f->middle->middle;
              f->middle->linfo = f->rinfo;
              /* f->rinfo = f->right->linfo; */
              f->middle->middle = f->right->left;
              if (f->middle->middle)
                f->middle->middle->father = f->middle;
              /* f->right->linfo = f->right->rinfo; */
              /* f->right->rinfo = nullptr; */
              /* f->right->left = f->right->middle; */
              /* f->right->middle = f->right->right; */
              /* f->right->right = nullptr; */
            }
            else { /* p == f->middle*/
              p->linfo = f->rinfo;
              /* f->rinfo = f->right->linfo; */
              p->middle = f->right->left;
              /* if (p->middle) */
              /*   p->middle->father = p; */
              /* f->right->linfo = f->right->rinfo; */
              /* f->right->rinfo = nullptr; */
              /* f->right->left = f->right->middle; */
              /* f->right->middle = f->right->right; */
              /* f->right->right = nullptr; */
            }
            f->rinfo = f->right->linfo;
            f->right->linfo = f->right->rinfo;
            f->right->rinfo = nullptr;
            f->right->left = f->right->middle;
            f->right->middle = f->right->right;
            f->right->right = nullptr;
            if (p->middle)
              p->middle->father = p;
            return;
          }

          if (f->rinfo) { /* father is 3-node*/
            if (p == f->left || p == f->middle) {
              if (p == f->left) {
                p->linfo = f->linfo;
                f->linfo = f->middle->linfo;
                p->middle = f->middle->left;
                if (p->middle)
                  p->middle->father = p;
                f->middle->left = f->middle->middle;
                /* f->middle->linfo = f->rinfo; */
                /* f->middle->rinfo = f->right->linfo; */
                /* f->middle->middle = f->right->left; */
                /* if (f->middle->middle) */
                /*   f->middle->middle->father = f->middle; */
                /* f->middle->right = f->right->middle; */
                /* if (f->middle->right) */
                /*   f->middle->right->father = f->middle; */
              }
              /* else{ */
              f->middle->linfo = f->rinfo;
              f->middle->rinfo = f->right->linfo;
              f->middle->middle = f->right->left;
              if (f->middle->middle)
                f->middle->middle->father = f->middle;
              f->middle->right = f->right->middle;
              /* if (f->middle->right) */
              /*   f->middle->right->father = f->middle; */
              /* } */
            }
            else { /* p == f->right */
              f->middle->rinfo = f->rinfo;
              f->middle->right = f->right->left;
              /* if (f->middle->right) */
              /*   f->middle->right->father = f->middle; */
            }
            if (f->middle->right)
              f->middle->right->father = f->middle;
            f->rinfo = nullptr;
            f->right->linfo = nullptr;
            delete f->right;
            f->right = nullptr;
            return;
          }

          /* father and sibling are 2-node */
          if (p == f->left) {
            p->linfo = f->linfo;
            p->rinfo = f->middle->linfo;
            p->middle = f->middle->left;
            if (p->middle)
              p->middle->father = p;
            p->right = f->middle->middle;
            if (p->right)
              p->right->father = p;
          }

          else { /* p == f->middle */
            f->left->rinfo = f->linfo;
            f->left->right = f->middle->left;
            if (f->left->right)
              f->left->right->father = f->left;
          }

          f->middle->linfo = nullptr;
          delete f->middle;
          f->middle = nullptr;
          p = f;
          f = f->father;
        }

        root = p->left;
        if (root)
          root->father = nullptr;
        p->linfo = nullptr;
        delete p;
        return;
      }
    }
  }
}

template <typename Key, typename Value>
int A23<Key, Value>::rank(Key key)
{
  a23node<Key, Value> *p = root;
  int r = 0;

  while (p) {
    if (*key < *(p->linfo->key))
      p = p->left;

    else if (*key > *(p->linfo->key)) {
      if (!(p->rinfo)) {
        r = r + 1 + countnodes(p->left);
        p = p->middle;
      }
      else {
        r = r + 1 + countnodes(p->left);

        if (*key < *(p->rinfo->key)) {
          p = p->middle;
        }
        else if (*key > *(p->rinfo->key)) {
          r = r + 1 + countnodes(p->middle);
          p = p->right;
        }
        else
          return r + countnodes(p->middle);
      }
    }
    else
      return r + countnodes(p->left);
  }
  return(r);
}

template <typename Key, typename Value>
Key A23<Key, Value>::seleciona(int k)
{
  a23node<Key, Value> *p = root;
  int r = countnodes(p->left);

  while (p) {
    if (k < r) {
      p = p->left;
      r--;
      if (p) {
        if (p->rinfo)
          r--;
        r = r - countnodes(p->middle) - countnodes(p->right);
      }
    }

    else if (k > r) {
      if (!(p->rinfo)) {
        p = p->middle;
        r++;
        if (p)
          r = r + countnodes(p->left);
      }
      else {
        r = r + 1 + countnodes(p->middle);

        if (k < r) {
          p = p->middle;
          r--;
          if (p) {
            if (p->rinfo)
              r--;
            r = r - countnodes(p->middle) - countnodes(p->right);
          }
        }
        else if (k > r) {
          p = p->right;
          r++;
          if (p)
            r = r + countnodes(p->left);
        }
        else
          return p->rinfo->key;
      }
    }
    else
      return p->linfo->key;
  }
  return nullptr;
}

template <typename Key, typename Value>
int A23<Key, Value>::countnodes(a23node<Key, Value> *p)
{
  int ans = 0;
  if (p) {
    ans = 1 + countnodes(p->left) + countnodes(p->middle);
    if (p->rinfo)
      ans = ans + 1 + countnodes(p->right);
  }
  return ans;
}


#if DEBUG == 1
template <typename Key, typename Value>
void A23<Key, Value>::print()
{
  cout << "\n";
  rprint(root, 0);
}

template <typename Key, typename Value>
void A23<Key, Value>::rprint(a23node<Key, Value> *p, int space)
{
  if (!p)
    return;

  rprint(p->right, space + 1);

  if (p->rinfo) {
    for (int i = 0; i < space; i++)
      cout << "   ";
    cout << *(p->rinfo->val) << ": " << *(p->rinfo->key); // << "\n";
    if (p->father)
      cout << "(" << *p->father->linfo->key << ")\n";
    else
      cout << "\n";
  }

  rprint(p->middle, space + 1);

  for (int i = 0; i < space; i++)
    cout << "   ";
  cout << *(p->linfo->val) << ": " << *(p->linfo->key);// << "\n";
  if (p->father)
    cout << "(" << *p->father->linfo->key << ")\n";
  else
    cout << "\n";

  rprint(p->left, space + 1);
}
#endif
